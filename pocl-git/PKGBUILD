# Maintainer : Alexandru Fikl <alexfikl at gmail dot com>
# Contributor : Sven-Hendrik Haase <svenstaro@gmail.com>
# Contributor : spider-mario <spidermario@free.fr>
# Contributor : Olaf Leidinger <oleid@mescharet.de>
# Contributor : fabien Cellier <fabien.cellier@gmail.com>

_pkgname=pocl
pkgname=pocl-git-yorick
pkgver=20250312.r9212
pkgrel=1
pkgdesc='Portable OpenCL an open-source implementation of OpenCL'
arch=('x86_64')
url="http://portablecl.org"
license=('MIT')
provides=('pocl')
conflicts=('pocl')
depends=(
  'clang' 'hwloc' 'opencl-icd-loader' 'spirv-llvm-translator'
  'onetbb' 'opencv' 'libjpeg-turbo')
makedepends=('llvm' 'cmake' 'opencl-headers' 'ocl-icd' 'ninja')
source=("https://github.com/${_pkgname}/${_pkgname}/archive/refs/tags/v${_pkgver}.tar.gz")
source=("git+https://github.com/pocl/pocl.git#branch=main")
sha512sums=('SKIP')

pkgver() {
  cd "${_pkgname}"

  date=$(git log --format='%cd' --date=short -1 | sed 's/-//g')
  rev=$(git rev-list --count HEAD)
  echo "${date}.r${rev}"
}

build() {
  cd "${_pkgname}"

  cmake -GNinja -Bbuild \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=lib \
    -DBUILD_SHARED_LIBS=ON \
    -DCMAKE_SKIP_RPATH=ON \
    -DCMAKE_BUILD_TYPE=Release \
    -DENABLE_TBB_DEVICE=ON \
    -DINSTALL_OPENCL_HEADERS=OFF \
    -DENABLE_TESTS=OFF \
    -DENABLE_EXAMPLES=OFF \
    -DKERNELLIB_HOST_CPU_VARIANTS=distro

  cmake --build build "${MAKEFLAGS:--j4}"
}

package() {
  cd "${_pkgname}"
  DESTDIR="${pkgdir}" cmake --install build

  install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}

# vim:set ts=2 sts=2 sw=2 et:
