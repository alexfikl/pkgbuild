# Maintainer : Alexandru Fikl <alexfikl at gmail dot com>
# Contributor : Kaizhao Zhang <zhangkaizhao@gmail.com>

_pkgname=filprofiler
pkgname=python-fil-profiler
pkgver=2024.11.2
pkgrel=1
pkgdesc='A Python memory profiler for data processing and scientific computing applications'
arch=('x86_64')
url="https://github.com/pythonspeed/filprofiler"
license=('Apache')
depends=('python-threadpoolctl' 'python-numpy' 'ipython')
makedepends=('python-setuptools-scm' 'python-setuptools-rust' 'rust' 'lld'
  'python-build' 'python-installer' 'python-wheel')
source=("${url}/archive/${pkgver}.tar.gz")
sha512sums=('cd691efe5a93bdc7db38fb1cb4b63ec77316029a261b26134d26be45df6400ec8b29099b4584d7c6ec821b7132b34b131dbc29fda8f8322758a4dad77190ddce')

prepare() {
  cd "${_pkgname}-${pkgver}"

  # replace gold with lld
  sed -i '35s/gold/lld/' filpreload/build.rs
  # remove `malloc_usable_size` from versionscript
  sed -i '14d' filpreload/versionscript.txt
}

build() {
  cd "${_pkgname}-${pkgver}"

  PYTHON_INCLUDE=$(python -c 'import sysconfig; print(sysconfig.get_paths()["include"])')
  export SETUPTOOLS_SCM_PRETEND_VERSION="${pkgver}"
  export CFLAGS="${CFLAGS} -I${PYTHON_INCLUDE} -ffat-lto-objects"
  export CXXFLAGS="${CXXFLAGS} -ffat-lto-objects"

  python -m build --wheel --skip-dependency-check --no-isolation .
}

package() {
  cd "${_pkgname}-${pkgver}"
  python -m installer --destdir="${pkgdir}" "dist/${_pkgname}"*.whl
}

# vim:set ts=2 sw=2 et:
