# Maintainer : Alexandru Fikl <alexfikl at gmail dot com>

_pkgname=vedo
pkgname=python-vedo
pkgver=2025.5.3
pkgrel=2
pkgdesc='A python module for scientific analysis of 3D objects based on VTK and Numpy'
arch=('any')
url="https://github.com/marcomusy/vedo"
license=('MIT AND OFL-1.0 AND CC0-1.0')
depends=('vtk' 'python-numpy' 'python-matplotlib' 'python-pygments')
makedepends=('python-build' 'python-installer' 'python-wheel')
source=("${_pkgname}-${pkgver}.tar.gz::${url}/archive/v${pkgver}.tar.gz")
sha512sums=('3da72a19cc820e2b7db58d66620c2d222967e6987d92248057df8332e1efef6032841bfe17f12a7763b93fb9519b3be57d9ca47847e11a763535e1616c821878')

build() {
  cd "${_pkgname}-${pkgver}"
  python -m build --wheel --skip-dependency-check --no-isolation .
}

package() {
  cd "${_pkgname}-${pkgver}"
  python -m installer --destdir="${pkgdir}" "dist/${_pkgname}-${pkgver}"*.whl

  install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}

# vim:set ts=2 sts=2 sw=2 et:
