LIB=../lib/libspherepack.a

UNAMES := $(shell uname -s)

F90 := gfortran -Ofast -fopenmp -std=legacy -L../lib -I../lib
CPP := gfortran -E
MAKE := make
AR := /usr/bin/ar
