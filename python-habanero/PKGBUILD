# Maintainer : Alexandru Fikl <alexfikl at gmail dot com>
# Contributor : JP-Ellis <josh@jpellis.me>

_pkgname=habanero
pkgname=python-habanero
pkgver=2.2.0
pkgrel=1
pkgdesc='Low level client for working with Crossref search API'
arch=('any')
url="https://github.com/sckott/habanero"
license=('MIT')
depends=('python-tqdm' 'python-httpx')
makedepends=('python-build' 'python-installer' 'python-wheel')
source=("${url}/archive/refs/tags/v${pkgver}.tar.gz")
sha512sums=('d9aee57bc8b20b51b2d66a309ba2e1ac8886d0c4d23071e97f50e14604f6ecc36bd0e2d31e7c0baa928891fa38f3369454a59e9f930b4ad3bf991a506377ccbd')

prepare() {
  cd "${_pkgname}-${pkgver}"

  # NOTE: removing to skip installing tests
  rm -rf test/__init__.py
}

build() {
  cd "${_pkgname}-${pkgver}"
  python -m build --wheel --skip-dependency-check --no-isolation .
}

package() {
  cd "${_pkgname}-${pkgver}"
  python -m installer --destdir="${pkgdir}" "dist/${_pkgname}-${pkgver}"*.whl

  install -Dm644 LICENSE.md "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}

# vim:set ts=2 sts=2 sw=2 et:
