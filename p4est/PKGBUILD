# Maintainer : Alexandru Fikl <alexfikl at gmail dot com>

# submodule versions
_libsc_pkgver=2.8.6

pkgname=p4est
pkgver=2.8.6
pkgrel=2
pkgdesc='A parallel AMR library based on octrees'
arch=('x86_64')
url="http://p4est.github.io/"
license=('GPL-2.0-or-later')
depends=('openmpi' 'metis' 'lapack' 'jansson')
makedepends=('git')
source=("git+https://github.com/cburstedde/p4est.git#tag=v${pkgver}"
  "git+https://github.com/cburstedde/libsc.git#tag=v${_libsc_pkgver}")
sha512sums=('6eab2d037d8548b9e13a920988719f17a6c8fa17f216a1163cd6db629863d77c45a9f7f9dc087c278548045227b77fd727ae82466edd50102096c41dd7b826eb'
  'b895297eb62698d0a26f99f1614094631784694d7ded2bf278baf1a0959822c7cdf9e249a856deb75cc76bcf6ed817b171142ac407e71dc285bb3fdd167812fd')

prepare() {
  cd "${pkgname}"

  git submodule init
  git config submodule.sc.url "${srcdir}/libsc"
  git -c protocol.file.allow=always submodule update sc

  ./bootstrap
}

build() {
  cd "${pkgname}"

  ./configure --prefix=/usr \
    --disable-static \
    --with-metis \
    --enable-vtk-doubles \
    --enable-openmp \
    --enable-mpi \
    --enable-mpiio

  make
}

package() {
  cd "${pkgname}"
  make DESTDIR="${pkgdir}" install

  rm -rf "${pkgdir}/usr/etc"
  install -Dm644 config/Makefile.p4est.mk -t "${pkgdir}/usr/share/${pkgname}"
  install -Dm644 sc/config/Makefile.sc.mk -t "${pkgdir}/usr/share/${pkgname}"
}

# vim:set ts=2 sts=2 sw=2 et:
