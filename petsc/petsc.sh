export PATH="$PATH:/opt/petsc/bin"
export PYTHONPATH="$PYTHONPATH:/opt/petsc/lib/python3.12/site-packages"

export PETSC_ARCH='linux-c-opt'
export PETSC_DIR="/opt/petsc/${PETSC_ARCH}"
