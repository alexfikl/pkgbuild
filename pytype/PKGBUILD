# Maintainer : Alexandru Fikl <alexfikl at gmail dot com>
# Contributor : Kaizhao Zhang <zhangkaizhao@gmail.com>

# submodules
_googletest_pkgver=1.14.0
_pybind11_pkgver=2.13.5

pkgname=pytype
pkgver=2024.10.11
pkgrel=1
pkgdesc='Python type inferencer'
arch=('x86_64')
url="https://www.github.com/google/pytype"
license=('Apache-2.0')
provides=('pytype')
depends=('python-attrs' 'python-importlab' 'python-libcst' 'python-networkx'
  'python-pydot' 'python-tabulate' 'python-toml' 'python-typed-ast'
  'python-typing_extensions' 'python-ninja' 'python-jinja'
  'python-immutabledict')
makedepends=('cmake' 'ninja' 'bison' 'flex' 'fakeroot'
  'python-build' 'python-installer' 'python-wheel')
options=(!emptydirs)
source=("git+${url}.git#tag=${pkgver}"
  "git+https://github.com/python/typeshed.git"
  "git+https://github.com/google/googletest.git#tag=v${_googletest_pkgver}"
  "git+https://github.com/pybind/pybind11.git#tag=v${_pybind11_pkgver}")
sha512sums=('06dc870187afd38df9c937f6cdde0a2ddb47a65e47278393b92046c6a42888092412763e8674a221e07da94c23bc9c2bc666729ca0dc255a0899399745f4b4da'
  'SKIP'
  '5abcdea64cf0e5f7548e8a74d377e561fb5199015995b3206565be0a113f8bb2972037e58069636bddd4022c923bb4f3af263e13294cbc5cbf5747432d1be0c9'
  'a45d3b3ae2406862caf8804f100db122210006fed399fe3ec6d7f73d5f29d8ef85adedb5c80e2019dbb41b0e9f5265089ae353b78916ef615073e71e2fcc6e1c')

prepare() {
  cd "${pkgname}"

  git submodule init
  git config submodule.typeshed.url "${srcdir}/typeshed"
  git config submodule.googletest.url "${srcdir}/googletest"
  git config submodule.pybind11.url "${srcdir}/pybind11"
  git -c protocol.file.allow=always submodule update \
    typeshed \
    googletest \
    pybind11
}

build() {
  cd "${pkgname}"

  python -m build --wheel --skip-dependency-check --no-isolation .
}

package() {
  cd "${pkgname}"
  python -m installer --destdir="${pkgdir}" dist/*.whl
}

# vim:set ts=2 sts=2 sw=2 et:
