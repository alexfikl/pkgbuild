# Maintainer : Alexandru Fikl <alexfikl at gmail dot com>
# Contributor : gigas002 <gigas002@pm.me>
# Contributor : Raziel23 <venom23 at runbox dot com>
# Contributor : Sandy Carter <bwrsandman@gmail.com>

pkgname=vcmi
pkgver=1.6.7
pkgrel=1
pkgdesc='Open-source engine for Heroes of Might and Magic III'
arch=('x86_64')
url="http://vcmi.eu"
license=('GPL-2.0-or-later')
depends=('boost-libs' 'ffmpeg' 'sdl2_image' 'sdl2_mixer' 'sdl2_ttf' 'qt6-base'
  'qt6-tools' 'libxkbcommon-x11' 'desktop-file-utils' 'gtk-update-icon-cache'
  'hicolor-icon-theme' 'onetbb' 'fuzzylite' 'luajit')
makedepends=('boost' 'cmake' 'ninja' 'git' 'ccache')
optdepends=('unshield: required by vcmibuilder'
  'unzip: required by vcmibuilder')
install="${pkgname}.install"
source=("git+https://github.com/vcmi/vcmi.git#tag=${pkgver}"
  "git+https://github.com/vcmi/innoextract.git#branch=vcmi")
sha512sums=('fd77578382030b223b634ac1bc9f7d3277f2793e0eae91de1f71f8560b0fac57f6791a4347fc74d4ba719463afe00872444ee57625d6c68c6e6f8adb8d41b4b3'
            'SKIP')

prepare() {
  cd "${pkgname}"

  git submodule init
  git config submodule.innoextract.url "${srcdir}/innoextract"
  git -c protocol.file.allow=always submodule update launcher/lib/innoextract
}

build() {
  cd "${pkgname}"

  cmake -GNinja -Bbuild \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_RPATH=/usr/lib/vcmi \
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE \
    -DCMAKE_SKIP_RPATH=FALSE \
    -DFORCE_BUNDLED_FL=OFF \
    -DCMAKE_BUILD_TYPE=Release \
    -DQT_VERSION_MAJOR=6 \
    -DENABLE_TEST=OFF

  cmake --build build "${MAKEFLAGS:--j4}"
}

package() {
  cd "${pkgname}"

  # NOTE: fix incorrect translation location
  mkdir -p build/launcher/translation
  mv build/launcher/*.qm build/launcher/translation

  mkdir -p build/mapeditor/translation
  mv build/mapeditor/*.qm build/mapeditor/translation

  DESTDIR="${pkgdir}" cmake --install build
}

# vim:set ts=2 sts=2 sw=2 et:
