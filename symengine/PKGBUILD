# Maintainer : Alexandru Fikl <alexfikl at gmail dot com>
# Contributor : Antonio Rojas <arojas@archlinux.org>
# Contributor : Michael Schubert <mschu.dev at gmail>

_pkgname=symengine
pkgname=symengine-yorick
pkgver=0.14.0
pkgrel=1
pkgdesc='Fast symbolic manipulation library, written in C++'
arch=('x86_64')
url='https://symengine.org/'
# url="https://github.com/symengine/symengine"
license=('MIT')
provides=('symengine')
conflicts=('symengine')
depends=(
  'flint' 'gcc-libs' 'glibc' 'libmpc' 'gmp' 'gmp-ecm' 'primesieve'
  'mpfr' 'llvm-libs')
makedepends=('cmake' 'ninja' 'boost' 'cereal')
source=(
  "https://github.com/symengine/symengine/archive/refs/tags/v${pkgver}.tar.gz"
  'https://gitlab.archlinux.org/archlinux/packaging/packages/symengine/-/raw/main/llvm-shared.patch')
sha512sums=('2b6012ed65064ff81c8828032c5a3148340582274e3604db2a43797ddbaa191520ed97da41efc2e842ba4a25326f53becc51f1e98935e8c34625bc5eaac8397f'
            '3624513f69f79235824d0420a77077fde92c6984e1c42b1eb8315e1c6b388239e92c6841535fa7cf4edb71eae31797c16959c7324e78d23abaa86f5c9e85f193')

prepare() {
  cd "${_pkgname}-${pkgver}"

  patch -p1 -i "${srcdir}/llvm-shared.patch"
}

build() {
  cd "${_pkgname}-${pkgver}"

  # NOTE: WITH_TCMALLOC=ON seems to cause issues with pybind11 built projects,
  # even pybind/python_example makes a script crash with invalid pointers

  cmake -GNinja -Bbuild \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS=ON \
    -DCMAKE_BUILD_TYPE=Release \
    -DWITH_TCMALLOC=OFF \
    -DWITH_PTHREAD=ON \
    -DWITH_SYMENGINE_THREAD_SAFE=ON \
    -DWITH_ECM=ON \
    -DWITH_LLVM=ON \
    -DWITH_MPFR=ON \
    -DWITH_MPC=ON \
    -DWITH_FLINT=ON \
    -DWITH_PRIMESIEVE=ON \
    -DWITH_BOOST=ON \
    -DWITH_COTIRE=OFF \
    -DWITH_SYSTEM_CEREAL=ON \
    -DBUILD_TESTS=OFF -DBUILD_BENCHMARKS=OFF

  cmake --build build "${MAKEFLAGS:--j4}"
}

package() {
  cd "${_pkgname}-${pkgver}"
  DESTDIR="${pkgdir}" cmake --install build

  install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

# vim:set ts=2 sts=2 sw=2 et:
