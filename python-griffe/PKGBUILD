# Maintainer : Alexandru Fikl <alexfikl at gmail dot com>
# Contributor : Astro Benzene <universebenzene at sina dot com>

_pkgname=griffe
pkgname='python-griffe'
pkgver=1.6.0
pkgrel=1
pkgdesc='Signatures for entire Python programs'
arch=('any')
url="https://mkdocstrings.github.io/griffe"
license=('ISC')
depends=('python-colorama')
makedepends=(
    'python-pdm-backend'
    'python-build'
    'python-installer'
    'python-wheel')
source=("https://github.com/mkdocstrings/griffe/archive/refs/tags/${pkgver}.tar.gz")
sha512sums=('8ce0565ba8c43afb262d7e65b2eab369b10a57d4855a226a83ffea60bd83d7f624d5f15876c51dd20861ba5d652c012245bd55dd87dd0beeda75305f26f330bf')

prepare() {
  cd "${_pkgname}-${pkgver}"

  # NOTE: Do not use the custom version setting
  sed -i 's/source = "call"/source = "scm"/g' pyproject.toml
  sed -i '59d' pyproject.toml
}

build() {
  cd "${_pkgname}-${pkgver}"

  export PDM_BUILD_SCM_VERSION="${pkgver}"
  python -m build --wheel --skip-dependency-check --no-isolation .
}

package() {
  cd "${_pkgname}-${pkgver}"

  python -m installer --destdir="${pkgdir}" "dist/${_pkgname//-/_}-${pkgver}"*.whl
  install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

# vim:set ts=2 sts=2 sw=2 et:
