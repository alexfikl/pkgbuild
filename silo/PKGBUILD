# Maintainer : Alexandru Fikl <alexfikl at gmail dot com>
# Contributor : eolianoe <eolianoe [at] gmail [DoT] com>
# Contributor : Carl Rogers <carl.rogers@gmail.com>
# Contributor : Jed Brown <jed@59A2.org>
# Contributor : Brenden Mervin <bmervin@utk.edu>

_pkgname=Silo
pkgname=silo
pkgver=4.11.1
pkgrel=7
pkgdesc='A mesh and field I/O library and scientific database'
url="https://github.com/LLNL/Silo"
arch=('x86_64')
license=('BSD-3-Clause' 'LicenseRef-LLNL')
depends=('hdf5-openmpi')
makedepends=('gcc-fortran')
source=("${url}/archive/refs/tags/${pkgver}.tar.gz")
sha512sums=('e2153f2e1cafa292f63d82af2ac2e53ce109aae8e8574fe1d994cf703db963d7804257151c3dac7b2a9eceb313eb1dd5a8b969d807437aafa075bc7ada21fdfc')

prepare() {
  cd "${_pkgname}-${pkgver}"

  autoreconf --force --install
}

build() {
  cd "${_pkgname}-${pkgver}"

  PY_INCLUDE=$(python -c 'import sysconfig; print(sysconfig.get_paths()["include"])')

  ./configure \
    CC=mpicc CXX=mpicxx \
    CXXFLAGS="${CXXFLAG} -std=c++98 -Wno-unused-result" \
    CFLAGS="${CFLAGS} -Wno-unused-result" \
    PYTHON_CPPFLAGS="-I${PY_INCLUDE}" \
    --prefix=/usr \
    --enable-shared \
    --enable-optimization \
    --enable-pythonmodule \
    --enable-fortran \
    --enable-install-lite-headers \
    --enable-hzip \
    --enable-fpzip \
    --with-zlib \
    --with-hdf5=/usr/include,/usr/lib

  make
}

package() {
  cd "${_pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install

  # rename browser
  mv "${pkgdir}/usr/bin/browser" "${pkgdir}/usr/bin/silo-browser"

  # install python
  sitepackages=$(python -c 'import site; print(site.getsitepackages()[0])')
  install -Dm755 "${pkgdir}/usr/lib/Silo.so" "${pkgdir}${sitepackages}/Silo.so"
  rm "${pkgdir}/usr/lib/Silo.so"
  rm "${pkgdir}/usr/lib/Silo.a"

  # license
  install -Dm644 COPYRIGHT "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
  install -Dm644 BSD_LICENSE_README "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE-BSD"
}

# vim:set ts=2 sts=2 sw=2 et:
