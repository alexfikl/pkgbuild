# PKGBUILD

A collection of personal PKGBUILD. Some of them are in the official repos and
these just have different compilation options, some are patched and some
just don't work.

Use at your own risk.

# Use

To create package, just use the standard way with `makepkg`:
```
$ makepkg -si
```
or the custom script from [pkgbuild.py](https://gitlab.com/alexfikl/my-random-scripts/-/blob/main/scripts/pkgbuild.py)
```
python pkgbuild.py path-to-pkgbuild
```
which will also try to sign the package, update `sha512sums` and other useful
things.

For more info see the wiki pages about [PKGBUILD files](https://wiki.archlinux.org/index.php/PKGBUILD)
and the [AUR](https://wiki.archlinux.org/index.php/Arch_User_Repository).

