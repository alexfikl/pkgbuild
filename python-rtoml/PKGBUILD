# Maintainer : Alexandru Fikl <alexfikl at gmail dot com>
# Container : KokaKiwi <kokakiwi+aur@kokakiwi.net>

_pkgname=rtoml
pkgname=python-rtoml
pkgver=0.11
pkgrel=1
pkgdesc='A better TOML library for Python implemented in Rust'
arch=('x86_64')
url="https://github.com/samuelcolvin/rtoml"
license=('MIT')
depends=('python')
makedepends=('maturin' 'python-installer')
options=('!lto')
source=("${url}/archive/v${pkgver}.tar.gz")
sha512sums=('65a440a57753e17d24db066872a3caedd48f42154fcfc1e5e471f65c0ef00e39ba535564d16f98ec20dab2bb87dde7c49e8d5a0f9647213acec41318f1e0ad52')

prepare() {
  cd "${_pkgname}-${pkgver}"
  cargo fetch --locked --target "$CARCH-unknown-linux-gnu"
}

build() {
  cd "${_pkgname}-${pkgver}"

  maturin build \
    --release \
    --strip \
    --locked \
    --target "$CARCH-unknown-linux-gnu" \
    --all-features
}

package() {
  cd "${_pkgname}-${pkgver}"
  python -m installer --destdir="${pkgdir}" "target/wheels/${_pkgname}-${pkgver}"*.whl

  install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}

# vim:set ts=2 sts=2 sw=2 et:
