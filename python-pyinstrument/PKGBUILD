# Maintainer : Alexandru Fikl <alexfikl at gmail dot com>
# Contributor : Kaizhao Zhang <zhangkaizhao@gmail.com>

_pkgname=pyinstrument
pkgname=python-pyinstrument
pkgver=5.0.1
pkgrel=1
pkgdesc='Call stack sampling profiler for Python'
arch=('x86_64')
url="https://github.com/joerick/pyinstrument"
license=('BSD-3-Clause')
depends=('python')
makedepends=('npm' 'python-build' 'python-installer' 'python-wheel')
source=("${url}/archive/v${pkgver}.tar.gz")
sha512sums=('6bfea27e592260f321c44d0ba91aebeebbfb4bbcc98f2bd30d42dc7fc4d2220c735d6503aedf0aced1099eb5befdf25745c95dd9758a8157a1647e80a1e2ba3c')

prepare() {
  cd "${_pkgname}-${pkgver}"

  # NOTE: removing to skip installing tests
  rm -rf test/__init__.py
}

build() {
  cd "${_pkgname}-${pkgver}"
  python -m build --wheel --skip-dependency-check --no-isolation .
}

package() {
  cd "${_pkgname}-${pkgver}"
  python -m installer --destdir="${pkgdir}" "dist/${_pkgname//-/_}-${pkgver}"*.whl

  install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}

# vim:set ts=2 sw=2 et:
