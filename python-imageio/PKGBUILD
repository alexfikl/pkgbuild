# Maintainer : Alexandru Fikl <alexfikl at gmail dot com>

_pkgname=imageio
pkgname=python-imageio
pkgver=2.37.0
pkgrel=1
pkgdesc='Python library for reading and writing image data'
arch=('any')
url="https://github.com/imageio/imageio"
license=('BSD-2-Clause')
depends=('python-numpy' 'python-pillow' 'python-lxml' 'python-scienceplots')
makedepends=('python-build' 'python-installer' 'python-wheel')
optdepends=('python-astropy: for FITS plugin'
  'python-gdal'
  'python-itk: for ITK formats'
  'python-imageio-ffmpeg: for working with video files')
source=("https://github.com/${_pkgname}/${_pkgname}/archive/v${pkgver}.tar.gz")
sha512sums=('24d57913e6f070d85591a7ed13362d17ff6011385bece7ad715a5b1d775202832028223eae1b6817bf30fc189eb136a88a969e882310742aa7cb2a1cd59ba310')

build() {
  cd "${_pkgname}-${pkgver}"
  python -m build --wheel --skip-dependency-check --no-isolation .
}

package() {
  cd "${_pkgname}-${pkgver}"
  python -m installer --destdir="${pkgdir}" "dist/${_pkgname}-${pkgver}"*.whl

  install -m644 -D LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}

# vim:set ts=2 sts=2 sw=2 et:
