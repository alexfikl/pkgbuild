_default:
    @just update

[doc("Check nvchecker sources for updates")]
update: check
    nvchecker -c .config/nvchecker.toml
    @echo -e "\e[1;32mChecking for updates done!\e[0m"

[doc("Update nvchecker versions and commit")]
commit:
    nvtake -c .config/nvchecker.toml --all
    git add .config
    git commit -m 'nvchecker: update versions'
    @echo -e "\e[0;32mNew versions have been updated!\e[0m"

[doc("Push all changes")]
push:
    git push origin main

[private]
check-sources:
    @echo -e "\e[0;32mChecking all configured sources exist...\e[0m"
    @for folder in $(rg -o -N -r '$1' '^\[([a-z0-9\-\.]+_*[a-z0-9]+)\]' .config/nvchecker.toml); do \
        [[ -d "${folder}" ]] || echo "folder '${folder}' does not exist"; \
    done

[private]
check-folders:
    @echo -e "\e[0;32mChecking all folders match a source...\e[0m"
    @for folder in $(ls -l | grep '^d' | awk '{ print $9; }'); do \
        rg -N "\[\"?${folder}\"?\]" .config/nvchecker.toml > /dev/null || echo "section '${folder}' not found"; \
    done;

[doc("Check matching nvchecker sources vs packages")]
check: check-sources check-folders

[doc("Reformat all scripts")]
format: justfmt shfmt

[doc("Run just --fmt over the justfiles")]
justfmt:
    just --unstable --fmt
    @echo -e "\e[1;32mjust --fmt clean!\e[0m"

[doc("Run shfmt over the source code")]
shfmt:
    fd PKGBUILD -x shfmt --write \
        --indent 2 \
        --space-redirects \
        --language-dialect bash
    @echo -e "\e[1;32mshfmt clean!\e[0m"
